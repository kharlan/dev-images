.PHONY: all no-pull use-cache

all: dockerfiles/*
	docker-pkg --info -c dockerfiles/config.yaml build dockerfiles

no-pull:
	docker-pkg --info -c dockerfiles/config.yaml build --no-pull dockerfiles

use-cache:
	docker-pkg --info -c dockerfiles/config.yaml build --use-cache --no-pull dockerfiles
